import axios from "axios"

export async function searchByGoogle(searchQuery: string){
  return axios.get("/api/search", {
    params: {
      searchQuery
    }
  })
}


export async function updateUserKeywords(userKeywords: string[]){
  return axios.put("/api/keyword", {
    userKeywords
  })
}


export async function getUserKeywords() {
  return axios.get("/api/keyword")
}