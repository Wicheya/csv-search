import { Button, Col, Row, Space } from "antd"
import { Header } from "antd/lib/layout/layout"
import { signIn, signOut, useSession } from "next-auth/react"
import UploadCsv from "../components/upload-csv"
import { LoginOutlined } from '@ant-design/icons';


export default function Index(){
  const {data : session} = useSession()

  const content = session
    ? (
      <>
        <Row justify="end" gutter={10}>
          <Col style={{ color : "white" }}>
            {session.user?.email}
          </Col>
          <Col>
          <Button onClick={() => signOut()} type="primary">
            signOut
          </Button>
          </Col>
        </Row>
      </>
    ) 
    : (
      <Row justify="end">
        <Col>
        <Button onClick={() => signIn()} type="primary">
            signIn
          </Button>
        </Col>
      </Row>
    )

    return (
      <>
        <Header className="header">
          {content}
        </Header>
        { session 
          ? <UploadCsv></UploadCsv>
          : (
            <Row align="middle" justify="center" style={{ height: 'calc(100vh - 64px)' }}>
              <Col style={{ textAlign: 'center' }}>
                <Space direction="vertical" size="large">
                  <LoginOutlined style={{ fontSize: '200px', color: 'lightgray' }} />
                  <h1 style={{ fontSize: '24px', color: 'gray' }}>
                    Sign in to use search service
                  </h1>
                </Space>
              </Col>
            </Row>
          )
        
        }
      </>
    )
}