import { uniq } from "lodash";
import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";
import prisma from "../../lib/prisma";

const handler:  NextApiHandler = async (req , res) => {
  const session = await getSession({req})

  if(!session){
    res.status(401)
    return
  }



  if(req.method === "GET") await getHandler(req, res, session)
  if(req.method === "PUT") await putHandler(req, res, session)
}


async function getHandler(req: NextApiRequest, res: NextApiResponse, session: Session){
  const userId = Number(session["userId"])
  const user = await prisma.user.findUnique({
    where: {
      id: userId
    }
  })

  return res.send({
    userKeywords : user?.searchKeywords || []
  })
}

async function putHandler(req: NextApiRequest, res: NextApiResponse, session: Session){
  const userId = Number(session["userId"])
  const user = await prisma.user.findUnique({
    where: {
      id: userId
    }
  })

  if(!user){
    return res.status(400)
  }

  const updateKeywords = uniq([...req.body.userKeywords, ...user.searchKeywords])
  await prisma.user.update({
    where: {
      id: userId,
    },
    data: {
      searchKeywords: updateKeywords
    }
  })
  return res.status(200)
}

export default handler;