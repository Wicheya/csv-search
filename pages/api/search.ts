import axios from "axios";
import { isEmpty } from "lodash";
import { NextApiHandler } from "next";
import { getSession } from "next-auth/react";
import prisma from "../../lib/prisma";

const handler : NextApiHandler = async (req , res) => {
  const session = await getSession({req})

  if(!session){
    res.status(401)
    return
  }

  const searchKeywordRecord = await prisma.searchKeyword.findUnique({
    where: {
      keyword: req.query.searchQuery as string
    }
  })

  if(isEmpty(searchKeywordRecord)){
    const searchResult: any = await axios.get("https://www.googleapis.com/customsearch/v1" , {
      params: {
        key: process.env.CUSTOM_SEARCH_API_KEY,
        cx: process.env.SEARCH_ENGINE_ID,
        q: req.query.searchQuery
      }
    }).catch(e => {
      console.log("ERROR" , e)
    })

    const upsertKeyword = await prisma.searchKeyword.upsert({
      where: {
        keyword: req.query.searchQuery as string,
      },
      update: {
        count: searchResult.data.items.length,
        totalSearchResult: Number(searchResult.data.searchInformation.totalResults),
        items: searchResult.data.items
      },
      create: {
        keyword: req.query.searchQuery as string,
        count: searchResult.data.items.length,
        totalSearchResult: Number(searchResult.data.searchInformation.totalResults),
        items: searchResult.data.items,
      },
    })

    res.send({
      count : upsertKeyword.count,
      totalSearchResult: upsertKeyword.totalSearchResult,
      items: upsertKeyword.items,
    })
  }else{
    res.send({
      count : searchKeywordRecord!.count,
      totalSearchResult: searchKeywordRecord!.totalSearchResult,
      items: searchKeywordRecord!.items,
    })
  }
  


}


export default handler;