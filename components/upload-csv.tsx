import { InboxOutlined } from '@ant-design/icons';
import { Collapse, message } from 'antd';
import Dragger from "antd/lib/upload/Dragger";
import { isEmpty } from "lodash";
import { parse } from "papaparse";
import { FC, ReactElement, useEffect, useState } from "react";
import { getUserKeywords, searchByGoogle, updateUserKeywords } from "../lib/fetch";
import { formatPrice } from '../lib/helper';


const { Panel } = Collapse;

const UploadCsv: FC<any> = (props) => {
  const [_ , forceRerender] = useState({})
  const [ userSearchKeywords , setUserSearchKeyWords ] = useState(new Set<string>())
  const [ searchResultDict , setSearchResultDict] = useState({})
  const [firstKeyword, setFirstKeyword] = useState();

  useEffect(() => {
    async function getFirstKeyword(){
      const response = await getUserKeywords()
      const firstKeyword = response.data?.userKeywords[0]
      if(isEmpty(firstKeyword)) return

      const searchResult = await searchByGoogle(firstKeyword)
      setFirstKeyword(firstKeyword)
      setSearchResultDict(state => {
        return { ...state , [firstKeyword] : searchResult.data }
      })
      setUserSearchKeyWords(state => {
        return new Set(response.data.userKeywords)
      })
    }

    getFirstKeyword()
  }, [])
  

  function handleFileChange(info){
    const { status } = info.file;

    if (status === 'done') {
      parseCsv(info.file.originFileObj)
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
    
  }

  function parseCsv(fileObj){
    let rowCount = 1
    const keywords: string[] = []
    parse<string>(fileObj , {
      step: (results, parser) => {
        if(rowCount++ > 100){
          parser.abort()
          return
        } 
  
        userSearchKeywords.add(results.data[0])
        keywords.push(results.data[0])
      },
      complete: (results, file) => {
        updateUserKeywords(keywords)
        forceRerender({})
        message.success(`${fileObj.name} file uploaded successfully.`);
      },
      error: (error , file) => {
        message.error(`${fileObj.name} file upload failed.`);
      }
    })
  }

  function getKeywordElements(searchKeywords: string[] | Set<string>){
    const elements: ReactElement[] = []
  
    for(let item of searchKeywords as Array<string>){
      elements.push((
        <Panel header={item} key={item}>
          <p>Total number of {item} links : {searchResultDict[item]?.count}</p>
          <p>Total search results of {item} : {formatPrice(searchResultDict[item]?.totalSearchResult)}</p>
        </Panel>
      ))
    }
  
    return elements
  }


  function onPanelChange(key: string){
    if(isEmpty(key)) return
    if(searchResultDict[key]) return
    
    searchByGoogle(key).then(response => {
      setSearchResultDict(state => {
        return {
          ...state , 
          [key] : response.data
        }
      })
    })
  }

  return (
    <>
      <Dragger multiple={false} accept=".csv" name="file" onChange={(info) => handleFileChange(info)}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">Click or drag file to this area to upload</p>
      </Dragger>

      <Collapse accordion onChange={(key) => onPanelChange(key as string) }>
        {getKeywordElements(userSearchKeywords)}
      </Collapse>
    </>
  )
}


export default UploadCsv



